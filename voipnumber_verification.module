<?php

/**
 *Implementation of hook_menu()
 */
function voipnumber_verification_menu() {
  $items['voipnumber_verification/send-code'] = array(
    'title' => 'Send Code',
    'page callback' => 'voipnumber_verification_send_code',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  $items['voipnumber_verification/verify-code'] = array(
    'title' => 'Verify Code',
    'page callback' => 'voipnumber_verification_verify_code',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  return $items;
}

//@todo: how to know its on voipnumber field?
function voipnumber_verification_form_field_ui_field_edit_form_alter(&$form, &$form_state) {
  if($form['#field']['module'] == 'voipnumberfield') {
    drupal_add_js(drupal_get_path('module', 'voipnumber_verification') . '/voipnumber_verification.js');

    $form['field']['settings']['voipnumber_verification'] = array(
      '#title' => t('VoIP Number Verification'),
      '#type' => 'fieldset',
    );

    $form['field']['settings']['voipnumber_verification']['voipnumber_verification_verify'] = array(
      '#title' => t('Must be verified'),
      '#type' => 'checkbox',
      '#default_value' => variable_get('voipnumber_verification_verify_' . $form['#field']['field_name'], FALSE),
    );

    $form['field']['settings']['voipnumber_verification']['voipnumber_verification_method'] = array(
      '#title' => t('Verify number via'),
      '#type' => 'radios',
      '#options' => array(
        1 => t('SMS'),
      ),
      '#disabled' => TRUE,
      '#default_value' => variable_get('voipnumber_verification_method_' . $form['#field']['field_name'], 1),
    );
    $form['field']['settings']['voipnumber_verification']['voipnumber_verification_text'] = array(
      '#title' => t('Verification text'),
      '#type' => 'textfield',
      '#default_value' => variable_get('voipnumber_verification_text_' . $form['#field']['field_name'], 'Your verification code is '),
    );

    $form['#submit'][] = 'voipnumber_verification_form_field_ui_field_edit_submit';
  }
}

function voipnumber_verification_form_field_ui_field_edit_submit(&$form, &$form_state) {
  $field_name = $form['#field']['field_name'];
  $voipnumber_verification_settings = $form_state['values']['field']['settings']['voipnumber_verification'];
  variable_set('voipnumber_verification_verify_' . $field_name, $voipnumber_verification_settings['voipnumber_verification_verify']);
  variable_set('voipnumber_verification_method_' . $field_name, $voipnumber_verification_settings['voipnumber_verification_method']);
  variable_set('voipnumber_verification_text_' . $field_name, $voipnumber_verification_settings['voipnumber_verification_text']);
}

function voipnumber_verification_form_node_form_alter(&$form, &$form_state) {
  drupal_add_js(drupal_get_path('module', 'voipnumber_verification') . '/voipnumber_verification.js');

  $finfo = _field_info_collate_fields();
  $ntype = $form['#node']->type;
  $fields = $finfo['instances']['node'][$ntype];

  foreach ($fields as $field) {
    //Filter only voipnumber fields which must to be verified
    if (isset($field['widget']) && $field['widget']['module'] == 'voipnumberfield') {
      $field_name = $field['field_name'];
      //Only convert if conversion is enabled for all fields or for this field.
      $must_be_verified = variable_get('voipnumber_verification_verify_' . $field_name, FALSE);
      if ($must_be_verified) {
        $form[$field_name]['#after_build'][] = 'voipnumber_verification_after_build';
      }
    }
  }

  $form['#validate'][] = 'voipnumber_verification_form_validate';
}

function voipnumber_verification_form_validate($form, &$form_state) {
  drupal_add_js(drupal_get_path('module', 'voipnumber_verification') . '/voipnumber_verification.js');
  $finfo = _field_info_collate_fields();
  $ntype = $form['#node']->type;
  $fields = $finfo['instances']['node'][$ntype];

  foreach ($fields as $field) {
    //Filter only voipnumber fields which must to be verified
    if (isset($field['widget']) && $field['widget']['module'] == 'voipnumberfield') {
      $field_name = $field['field_name'];

      //Only convert if conversion is enabled for all fields or for this field.
      $must_be_verified = variable_get('voipnumber_verification_verify_' . $field_name, FALSE);
      if ($must_be_verified) {
        $langcode = $form[$field_name]['#language'];
        foreach ($form_state['values'][$field_name][$langcode] as $delta => $phone_number_field) {
          if (is_array($phone_number_field)) {
            if(!empty($phone_number_field['real_vnid'])) {
              $voipnumber = VoipNumber::load($phone_number_field['real_vnid']);
              if(($phone_number_field['vnid'] != $voipnumber->getNumber())
                || !$voipnumber->isVerified()) {
                //Must check the key
                $key = _voipnumber_verification_generate_key($field_name, $delta, $phone_number_field['vnid']);
                if (!isset($_SESSION['voipnumber_verification_verified'][$key])) {
                  form_set_error($field_name. '][' . $langcode . '][' . $delta . '][vnid', 'Phone number must be verified.');
                }
              }
            }
            else if (isset($phone_number_field['vnid']) && !empty($phone_number_field['vnid'])) {
              //Check if the number was verified
              $key = _voipnumber_verification_generate_key($field_name, $delta, $phone_number_field['vnid']);
              if (!isset($_SESSION['voipnumber_verification_verified'][$key])) {
                //If not verified display error
                form_set_error($field_name. '][' . $langcode . '][' . $delta . '][vnid', 'Phone number must be verified.');
              }
            }
          }
        }
      }
    }
  }
}

/**
 *Implementation of hook_node_insert()
 */
function voipnumber_verification_node_insert($node) {
  _voipnumber_verification_node_save($node);
}

/**
 *Implementation of hook_node_insert()
 */
function voipnumber_verification_node_update($node) {
  _voipnumber_verification_node_save($node);
}

function _voipnumber_verification_node_save(&$node) {
  $finfo = _field_info_collate_fields();
  $ntype = $node->type;
  $fields = $finfo['instances']['node'][$ntype];

  foreach ($fields as $field) {
    //Filter only voipnumber fields which must to be verified
    if (isset($field['widget']) && $field['widget']['module'] == 'voipnumberfield') {
      $field_name = $field['field_name'];
      $must_be_verified = variable_get('voipnumber_verification_verify_' . $field_name, FALSE);
      if ($must_be_verified) {
        //Mark all voipnumbers as verified
        $fields = field_get_items('node', $node, $field_name);
        //foreach ($node->{$field_name}[$node->language] as $delta => $phone_number_field) {
        foreach($fields as $delta=>$phone_number_field) {
          if (isset($phone_number_field['vnid'])) {
            //Save VoipNumber as verified
            $voipnumber = VoipNumber::load($phone_number_field['vnid']);
            $voipnumber->setVerified();
            $voipnumber->save();
            //Remove key from user session
            $key = _voipnumber_verification_generate_key($field_name, $delta, $voipnumber->getNumber());
            unset($_SESSION['voipnumber_verification_verified'][$key]);
          }
        }
      }
    }
  }
}

function voipnumber_verification_after_build($form, &$form_state) {
  $langcode = $form['#language'];
  $field_name = $form[$langcode]['#field_name'];
  if (variable_get('voipnumber_verification_method_' . $field_name, 1) === 1) {
    $method = 'SMS';
  }

  for ($i = 0; $i <= $form[$langcode]['#max_delta']; $i++) {
    $display_verified = 'style="display:none;"';
    if($form_state['values'][$field_name][$langcode][$i]['real_vnid']) {
       $voipnumber = VoipNumber::load($form_state['values'][$field_name][$langcode][$i]['real_vnid']);
       if(($form_state['values'][$field_name][$langcode][$i]['vnid'] == $voipnumber->getNumber())
         && $voipnumber->isVerified()) {
         $display_verified = '';
       }
    }
    if($form_state['values'][$field_name][$langcode][$i]['vnid']) {
      $key = _voipnumber_verification_generate_key($field_name, $i, $form_state['values'][$field_name][$langcode][$i]['vnid']);
      if (isset($_SESSION['voipnumber_verification_verified'][$key])) {
        $display_verified = '';
      }
    }
    //$display_verified
    $form[$langcode][$i]['verify'] = array(
      '#type' => 'markup',
      '#weight' => 50,
      '#markup' => '<div class="voipnumber-verification-wrapper" data-field_name="' . $field_name . '" data-delta="' . $i . '">
      <div class="messages status verified" '.$display_verified.'>Your number has been verified.</div>
      <span class="verify-code-wrapper" style="display:none;">
      <p>We have just sent you a verification code via SMS. Please type in the verification code here:</p>
      <input type="text" class="code form-text"/>
      <a class="button verify-code">Submit</a>
      </span>
      <a class="button verify-number">Verify number via ' . $method . '</a></div>',
    );
  }

  return $form;
}

function voipnumber_verification_send_code() {
  $phone_number = $_GET['phone_number'];
  $field_name = $_GET['field_name'];
  $delta = $_GET['delta'];
  $key = _voipnumber_verification_generate_key($field_name, $delta, $phone_number);

  if ($phone_number && $field_name && $delta != NULL) {
    //4-digit random code
    $code = rand(pow(10, 3), pow(10, 4) - 1);

    //save code in session
    $_SESSION['voipnumber_verification_codes'][$key] = $code;

    //watchdog('debug', "$key code: " . $code);

    $call = new VoipCall();
    $call->setDestNumber($phone_number);
    $text = variable_get('voipnumber_verification_text_' . $field_name, 'Your verification code is ') . $code;
    voip_text($text, $call);

    $response = array('status' => 200);
  }
  else {
    $response = array('status' => 500, 'message' => 'Server error.');
  }
  drupal_json_output($response);
}

function voipnumber_verification_verify_code() {
  $phone_number = $_GET['phone_number'];
  $field_name = $_GET['field_name'];
  $delta = $_GET['delta'];
  $code = $_GET['code'];

  $key = _voipnumber_verification_generate_key($field_name, $delta, $phone_number);

  if ($_SESSION['voipnumber_verification_codes'][$key] == $code) {
    $_SESSION['voipnumber_verification_verified'][$key] = TRUE;
    $response = array('verified' => TRUE);
  }
  else {
    $response = array('verified' => FALSE);
  }
  drupal_json_output($response);
}

function _voipnumber_verification_generate_key($field_name, $delta, $phone_number) {
  return $field_name . "-" . $delta . "-" . $phone_number;
}
